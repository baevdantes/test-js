import $ from "jquery";

$("#ya_map").on("load", function () {
  ymaps.ready(function () {
    const myMap = new ymaps.Map('map', {
        center: [54.7346, 55.9765],
        zoom: 16,
        controls: [],
      },
      {
        suppressMapOpenBlock: true,
      }),
      myPlacemark = new ymaps.Placemark([54.7346, 55.9765]);
    myMap.geoObjects
      .add(myPlacemark);
    myMap.behaviors
      .disable(['drag', 'scrollZoom'])
  });
});