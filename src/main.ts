import $ from "jquery";
import "./main.scss";
import User from "./user";
import "./map"

const callBack = $("#callback");

callBack.click(function (event: Event) {
    event.preventDefault();
});

setTimeout(() => {
    $(".body").css("display", "block");
}, 5);

/* Some examples */

const userMike = new User("Mike", "male", 180);

console.log(userMike.name);
console.log(userMike.role);
console.log(userMike.sex);
userMike.greet();

/* Loading page */

$(window).on("load", () => {
    setTimeout(() => {
        $(".body").removeClass("body_fixed");
    }, 0);
    setTimeout(() => {
        $(".preloader").removeClass("preloader_active");
    }, 0);
});

/* Tabs */

$(".price__tab-item").click(function () {
    $(".price__tab-item").removeClass("price__tab-item_active");
    $(".price__content-item").removeClass("price__content-item_active");
    $(this).addClass("price__tab-item_active");
    const tabTarget: any = $(this).attr("data-target");
    $(`.price__content-item[data-price=${tabTarget}]`).addClass("price__content-item_active");
});

$(".calc__tab-item").click(function () {
    $(".calc__tab-item").removeClass("calc__tab-item_active");
    $(".calc__tab-content").removeClass("calc__tab-content_active");
    $(this).addClass("calc__tab-item_active");
    const tabTarget: any = $(this).attr("data-target");
    $(`.calc__tab-content[data-calc=${tabTarget}]`).addClass("calc__tab-content_active");
});

/* Replace content in modal */

const modals: any = {
    "calc_desktop_power": {
        title: "Компьютер не включается / не работает",
        image: "assets/img/desktop__offpower.png",
        text: "Основная причина того, что ваш компьютер не включается, - это проблема в блоке питания.",
        price: 450,
    },
    "calc_desktop_low": {
        title: "Компьютер тормозит / медленно работает",
        image: "assets/img/desktop__low.png",
        text: "<p class='popup__paragraph'>Основная причина того, что ваш компьютер не включается, - это проблема в блоке питания.</p>" +
            "<p class='popup__paragraph'>Основная причина того, что ваш компьютер не включается, - это проблема в блоке питания.</p>",
        price: 650,
    },
    "calc_desktop_wifi": {
        title: "Компьютер тормозит / медленно работает",
        image: "assets/img/desktop__low.png",
        text: "<p class='popup__paragraph'>Основная причина того, что ваш компьютер не включается, - это проблема в блоке питания.</p>" +
            "<p class='popup__paragraph'>Основная причина того, что ваш компьютер не включается, - это проблема в блоке питания.</p>",
        price: 650,
    },
};

/* Nav mobile */

let windowOffset: number;

$(".header__burger-svg").click(function () {
    windowOffset = $(window).scrollTop();
    $(".body").addClass("body_fixed");
    $(".header__nav-wrapper").addClass("header__nav-wrapper_active");
});

$(".nav__close").click(function () {
    $(".body").removeClass("body_fixed");
    setTimeout(() => {
        $('body,html').animate({
            scrollTop: windowOffset
        }, 0);
    }, 50);
    setTimeout(() => {
        $(".header__nav-wrapper").removeClass("header__nav-wrapper_active");
    }, 100);
});

const popup = $("#popup");

if (window.innerWidth <= 992) {
    $(".js_modal").click(function () {
        windowOffset = $(window).scrollTop();
        $(".body").addClass("body_fixed");
        const modalTarget = $(this).attr("data-target");
        const modalTargetImage = $(this).find(".calc__problem-img").attr("src");
        $(".popup__img-item").attr("src", modalTargetImage);
        popup.find(".popup__title").text(modals[modalTarget].title);
        popup.find(".popup__text").html(modals[modalTarget].text);
        popup.find(".popup__price-number").text(modals[modalTarget].price);
        popup.addClass("popup_active");
        $(".overlay").addClass("overlay_active");
    });

    $(".overlay, .popup__close").click(function () {
        $(".body").removeClass("body_fixed");
        setTimeout(() => {
            $('body,html').animate({
                scrollTop: windowOffset
            }, 0);
        }, 50);
        setTimeout(() => {
            $(".popup").removeClass("popup_active");
            $(".overlay").removeClass("overlay_active");
        }, 100);
    });
} else {
    $(".js_modal").click(function () {
        const modalTarget = $(this).attr("data-target");
        const modalTargetImage = $(this).find(".calc__problem-img").attr("src");
        $(".popup__img-item").attr("src", modalTargetImage);
        popup.find(".popup__title").text(modals[modalTarget].title);
        popup.find(".popup__text").html(modals[modalTarget].text);
        popup.find(".popup__price-number").text(modals[modalTarget].price);
        popup.addClass("popup_active");
        $(".overlay").addClass("overlay_active");
    });

    $(".overlay, .popup__close").click(function () {
        $(".overlay").removeClass("overlay_active");
        popup.removeClass("popup_active");
    });
}

/* Nav links */

if (window.innerWidth <= 992) {
    $(".js_scroll").click(function (event: any) {
        event.preventDefault();
        const id = $(this).attr('href');
        const top = $(id).offset().top - 60;
        $(".body").removeClass("body_fixed");
        setTimeout(() => {
            if (id === "#calc") {
                $('body,html').animate({
                    scrollTop: top
                }, 0);
            } else {
                $('body,html').animate({
                    scrollTop: top - 20
                }, 0);
            }
        }, 50);
        setTimeout(() => {
            $(".header__nav-wrapper").removeClass("header__nav-wrapper_active");
        }, 100);
    });
} else {
    $(".js_scroll").click(function (event: any) {
        event.preventDefault();
        const id = $(this).attr('href');
        const top = $(id).offset().top - 80;
        if (id === "#calc") {
            $('body,html').animate({
                scrollTop: top
            }, 0);
        } else {
            $('body,html').animate({
                scrollTop: top - 20
            }, 0);
        }
    });
}


