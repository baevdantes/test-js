interface IHuman {
  sex: string;
  height?: number;
}

interface IUser extends IHuman {
  name: string;
  role: string;
}

class Human implements IHuman {
  sex: string;
  height: number;

  constructor(sex: string, height: number) {
    this.sex = sex;
    this.height = height;
  }
}

export default class User extends Human implements IUser {
  name: string;
  role: string = "Admin";

  constructor(name: string, male: string, height: number) {
    super(male, height);
    this.name = name;
  }

  greet(): void {
    console.log(`Hello, ${this.name}!`);
    console.log(this);
  }
}